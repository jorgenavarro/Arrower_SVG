# Arrower

Takes a GenBank file and writes a vector figure in SVG format where genes are represented as arrows.

Optionally, it can take pfd output files from [BiG-SCAPE](https://git.wageningenur.nl/medema-group/BiG-SCAPE), (which are themselves processed from hmmscan output) with domain predictions, so they can be written inside the arrows. By the time being, the colors are chosen randomly (and written in a separate file, so subsequent runs of the script will pick the same colors). Genes with no name are colored grey.

It can also write the SVG(s) from your file(s) in a single html page.

Example:
![Biosynthetic Gene Cluster 55 from the MIBiG database](BGC0000055.svg)

See complete options with `python Arrower.py -h`.

Adapted from Peter Cimermančič's [original code](https://github.com/petercim/Arrower)
